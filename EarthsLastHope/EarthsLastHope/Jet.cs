﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Net;
using Microsoft.Xna.Framework.Storage;
//using AsimovManagerLib;
using SlimDX.DirectInput;

namespace EarthsLastHope
{
    class Jet : Player
    {
        public int nbMunition = 300;
        public int health = 100;

        //public Quaternion rotation = Quaternion.Identity;
        public int altitude;
        public float moveSpeed = 50f;
        public static int MIN_SPEED = 35;
        public static int MAX_SPEED = 100;
        public bool isExploding = false;
        public int frameExplodingCount = 0;
        //public float camDecalZ = 0;

        public List<Bullet> bulletList = new List<Bullet>();
        public double lastBulletTime = -1000000000;
        bool fireSide = true;
        public Texture2D muzzleFlash;
        public bool flash = false;
        /*Quaternion cameraRotation = Quaternion.Identity;
       
        public Matrix viewMatrix;
        //public Matrix projectionMatrix;

        public Vector3 cameraPosition;
        public Vector3 cameraLookAt;
        public Vector3 camup;
        public Vector3 cambak;*/
        public Launcher launcher;
        public Vector3 jetRight;
        public Vector3 jetUp;
        public Vector3 jetBack;

        //SIMULATEUR//
        //public AsimovManager manager;

        //public static bool isActuateur = false;


        //public Vector3 gauche;
        //public Vector3 droite;
        //**********************//

        public static int NB_MAX_MISSILES = 4;
        public static int MISSILE_MAX_SPEED = 140;
        public float[] missileInitPos = new float[4];
        public double lastMissileTime = -1;
        public Texture2D smokePuff;
        public Vector3 target;

        //public SoundEffect soundGun;

        //BasicEffect basicEffect;

        public int getMissilesCount()
        {
            return launcher.getMissilesCount();
        }


        public Jet(Vector3 position, Vector3 scale, ParticleSystem particle)
            : base(position, scale, Quaternion.Identity)
        {
            //Reset();
            launcher = new Launcher(scale, particle);
            isExploding = false;
            frameExplodingCount = 0;
            target = Vector3.Zero;
        }

        public void Initialize()
        {
            launcher.ReloadMissiles();
        }

        public void Reset()
        {
            position = new Vector3(0, 24000, 50000);
            rotation = Quaternion.Identity;
            isExploding = false;
            frameExplodingCount = 0;
            //camDecalZ = 0;
        }

        public void Explode()
        {
            isExploding = true;
            //camDecalZ = 20;

            if (frameExplodingCount >= 100)
            {
                Reset();
            }
            frameExplodingCount++;
        }

        public void Update(GameTime gameTime)
        {
            if (!isExploding)
            {
                MoveForward();
                ProcessKeyboard(gameTime);
            }

            UpdateBulletPositions();


            altitude = (int)(this.position.Y / 100);

            jetUp = new Vector3(0, 1, 0);
            jetUp = Vector3.Transform(jetUp, Matrix.CreateFromQuaternion(rotation));
            jetUp.Normalize();

            jetBack = new Vector3(0, 0, -1);
            jetBack = Vector3.Transform(jetBack, Matrix.CreateFromQuaternion(rotation));
            jetBack.Normalize();

            jetRight = new Vector3(1, 0, 0);
            jetRight = Vector3.Transform(jetRight, Matrix.CreateFromQuaternion(rotation));
            jetRight.Normalize();

            launcher.Update(gameTime, position, rotation, jetRight);


            //UpdateCamera();
        }

        private void UpdateBulletPositions()
        {
            foreach (Bullet bullet in bulletList)
            {
                bullet.Update();
            }
            /*for (int i = 0; i < bulletList.Count; i++)
            {
                bu
                dfsf
                Bullet currentBullet = bulletList[i];
                MoveForward(ref currentBullet.position, currentBullet.rotation, moveSpeed * 2.0f);

                bulletList[i] = currentBullet;
            }*/
        }

        private void MoveForward()
        {
            Vector3 addVector = Vector3.Transform(new Vector3(0, 0, -1), rotation);
            position += addVector * moveSpeed;
        }

        public override void Draw(Camera cam)
        {
            Matrix worldMatrix = Matrix.CreateScale(scale) * Matrix.CreateRotationY(MathHelper.Pi) * Matrix.CreateFromQuaternion(rotation) * Matrix.CreateTranslation(position);

            Matrix[] xwingTransforms = new Matrix[model.Bones.Count];
            model.CopyAbsoluteBoneTransformsTo(xwingTransforms);
            foreach (ModelMesh mesh in model.Meshes)
            {
                foreach (BasicEffect effect in mesh.Effects)
                {
                    effect.EnableDefaultLighting();

                    effect.World = xwingTransforms[mesh.ParentBone.Index] * worldMatrix;
                    effect.Projection = cam.projectionMatrix;
                    effect.View = cam.viewMatrix;
                }
                mesh.Draw();
            }

            launcher.DrawMissiles(cam);
            foreach (Bullet bullet in bulletList)
            {
                bullet.Draw(cam);
            }
        }


        private void ProcessKeyboard(GameTime gameTime)
        {
            float Zrot = 0;
            float Xrot = 0;
            float Yrot = 0;

            float turningSpeed = (float)gameTime.ElapsedGameTime.TotalMilliseconds / 800.0f;

            Microsoft.Xna.Framework.Input.KeyboardState keys = Microsoft.Xna.Framework.Input.Keyboard.GetState();

            if (keys.IsKeyDown(Keys.D))
                Zrot += turningSpeed * 1.5f;
            if (keys.IsKeyDown(Keys.A))
                Zrot -= turningSpeed * 1.5f;

            if (keys.IsKeyDown(Keys.S))
                Xrot += turningSpeed;
            if (keys.IsKeyDown(Keys.W))
                Xrot -= turningSpeed;

            if (keys.IsKeyDown(Keys.Left))
                Yrot += turningSpeed * 0.15f;
            if (keys.IsKeyDown(Keys.Right))
                Yrot -= turningSpeed * 0.15f;

            if (keys.IsKeyDown(Keys.Up))
            {
                moveSpeed += 1f;

                if (moveSpeed > MAX_SPEED)
                {
                    moveSpeed = MAX_SPEED;
                }
            }
            if (keys.IsKeyDown(Keys.Down))
            {
                moveSpeed -= 1f;
                if (moveSpeed < MIN_SPEED)
                {
                    moveSpeed = MIN_SPEED;
                }
            }

            if (keys.IsKeyDown(Keys.Space))
            {
                FireBullets(gameTime);
            }

            if (keys.IsKeyDown(Keys.M))
            {
                launcher.FireMissile(gameTime, moveSpeed, target);
            }

            // POUR LES TESTS //
            if (keys.IsKeyDown(Keys.R))
            {
                launcher.ReloadMissiles();
                nbMunition = 300;
            }
            //***********************//

            Quaternion additionalRot = Quaternion.CreateFromAxisAngle(new Vector3(0, 0, -1), Zrot) * Quaternion.CreateFromAxisAngle(new Vector3(1, 0, 0), Xrot) * Quaternion.CreateFromAxisAngle(new Vector3(0, 1, 0), Yrot);
            rotation *= additionalRot;
        }

        private void FireBullets(GameTime gameTime)
        {
            double currentTime = gameTime.TotalGameTime.TotalMilliseconds;

            if (nbMunition > 0)
            {
                if (currentTime - lastBulletTime > 50)
                {
                    Vector3 bulletPosition = new Vector3();

                    //newBullet.bulletback = jetBack;
                    //newBullet.rotation = rotation;

                    Vector3 correctionX = jetRight * 7;
                    Vector3 correctionZ = jetBack * 60;

                    if (fireSide)
                        bulletPosition = position + correctionX + correctionZ;
                    else
                        bulletPosition = position - correctionX + correctionZ;

                    fireSide = !fireSide;

                    Bullet newBullet = new Bullet(bulletPosition, rotation);
                    newBullet.bulletback = jetBack;

                    bulletList.Add(newBullet);
                    nbMunition--;
                    SoundManager.GetSound("jet.gun").Play();

                    flash = true;

                    lastBulletTime = currentTime;
                }
            }
        }

    }
}
