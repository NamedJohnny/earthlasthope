﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace EarthsLastHope
{
    class MotherShip : Enemy
    {
        public MotherShip(Vector3 position)
            : base(position)
        {
            this.pointWorth = 10000;
            this.rayon = 10000;
            this.rotationSpeed = 5000;
            this.scale = new Vector3(20, 20, 20);
        }

        public void Patrol(Vector3 patrolLocation)
        {
            position.X = (float)(patrolLocation.X + (rayon * Math.Cos(angle * 2 * MathHelper.Pi / rotationSpeed)));
            position.Z = (float)(patrolLocation.Z + (rayon * Math.Sin(angle * 2 * MathHelper.Pi / rotationSpeed)));
            rotation.Y -= MathHelper.ToRadians(perpendicularAngle);
            angle++;
        }

        public void Follow(GameObject target)
        {

        }

        public void Attack(GameObject target)
        {

        }
    }
}
