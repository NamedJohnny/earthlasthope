﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Net;
using Microsoft.Xna.Framework.Storage;

namespace EarthsLastHope
{
    public class Missile : GameObject
    {
        const float trailParticlesPerSecond = 200;
        const int numExplosionParticles = 30;
        const int numExplosionSmokeParticles = 50;
        const float projectileLifespan = 1.5f;
        const float sidewaysVelocityRange = 60;
        const float verticalVelocityRange = 40;
        const float gravity = 15;

        ParticleEmitter trailEmitter;

        public static int NB_SMOKEPUFF = 125;
        public Quaternion rotationQuat;
        public float moveSpeed = 0;
        public bool isFired = false;
        protected int TTL = 500;
        public Bullet[] SmokeTrail = new Bullet[NB_SMOKEPUFF];

        public Effect effect;

        public bool isActive = true;
        public Vector3 target;
        private int nbLastFrame = NB_SMOKEPUFF;
        private float slerpAmount = 0.001f;


        public Missile(Model model, Vector3 scale, ParticleSystem particle)
            : base(new Vector3(), scale)
        {
            // Use the particle emitter helper to output our trail particles.
            trailEmitter = new ParticleEmitter(particle, trailParticlesPerSecond, position);
            this.model = model;
            initMissileSmoke();
        }

        public void initMissileSmoke()
        {
            Random Alea = new Random();

            int tempCamUpX;
            int tempCamUpY;
            int tempCamUpZ;
            for (int i = 0; i < NB_SMOKEPUFF; i++)
            {
                tempCamUpX = Alea.Next(2000) - 1000;
                tempCamUpY = Alea.Next(2000) - 1000;
                tempCamUpZ = Alea.Next(2000) - 1000;
                //SmokeTrail[i] = new Bullet();
                //SmokeTrail[i].camUp = new Vector3(tempCamUpX, tempCamUpY, tempCamUpZ);
            }
        }

        public void Update(GameTime gameTime, Vector3 basePosition, Quaternion rotation)
        {
            if (!isFired && isActive)
            {
                this.position = basePosition;
                rotationQuat = rotation;
            }
            else
            {
                if (isActive)
                {
                    if (TTL <= 0)
                        isActive = false;
                    else
                        TTL--;

                    if (target == Vector3.Zero)
                    {
                        if (moveSpeed < Jet.MISSILE_MAX_SPEED)
                            moveSpeed += 1f;
                    }
                    else
                    {
                        if (moveSpeed < Jet.MISSILE_MAX_SPEED)
                            moveSpeed += 1f;
                        if (slerpAmount < 0.15f)
                            slerpAmount += 0.001f;

                        rotationQuat = Quaternion.Slerp(rotationQuat, GetRotation(this.position, target), slerpAmount);
                    }

                    trailEmitter.Update(gameTime, this.position);
                    MoveForward();
                }
            }
        }

        public override void Draw(Camera cam)
        {
            if (isActive)
            {
                Matrix worldMatrix = Matrix.CreateScale(scale) * Matrix.CreateRotationY(MathHelper.Pi) * Matrix.CreateFromQuaternion(rotationQuat) * Matrix.CreateTranslation(position);

                Matrix[] missileTransforms = new Matrix[model.Bones.Count];
                model.CopyAbsoluteBoneTransformsTo(missileTransforms);
                foreach (ModelMesh mesh in model.Meshes)
                {
                    foreach (BasicEffect effect in mesh.Effects)
                    {
                        effect.EnableDefaultLighting();

                        effect.World = missileTransforms[mesh.ParentBone.Index] * worldMatrix;
                        effect.Projection = cam.projectionMatrix;
                        effect.View = cam.viewMatrix;
                    }
                    mesh.Draw();
                }
            }
        }

        private void MoveForward()
        {
            Vector3 addVector = Vector3.Transform(new Vector3(0, 0, -1), rotationQuat);
            position += addVector * moveSpeed;
        }

        private Quaternion GetRotation(Vector3 origine, Vector3 destination)
        {
            Vector3 angle = destination - origine;
            angle.Normalize();

            Matrix laRotation = Matrix.CreateLookAt(Vector3.Zero, angle, Vector3.Up);
            laRotation = Matrix.Transpose(laRotation);
            return Quaternion.CreateFromRotationMatrix(laRotation);
        }

        public void Fire(Vector3 target, float speed)
        {
            isFired = true;
            moveSpeed = speed;
            if (target != null)
                this.target = target;
        }

    }
}
