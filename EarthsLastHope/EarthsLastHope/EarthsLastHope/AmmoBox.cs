﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace EarthsLastHope
{
    class AmmoBox : GameObject
    {
        public AmmoBox(Vector3 position, Vector3 scale) : base(position, scale)
        {

        }

        public void Reset(int maxX, int minX, int maxZ, int minZ)
        {
            Random rand = new Random();
            float randomX = rand.Next(maxX) - minX;
            float randomZ = rand.Next(maxZ) - minZ;

            position.X = randomX;
            position.Y = 14000;
            position.Z = randomZ;
        }

        public void Update()
        {
            position.Y -= 8;
            /*if () {
                  parachute.Reset((ESPACEMENTX * NB_BATIMENTX), ((ESPACEMENTX * NB_BATIMENTX) / 2), (ESPACEMENTZ * NB_BATIMENTZ), ((ESPACEMENTZ * NB_BATIMENTZ) / 2));
            }*/
        }
    }
}
