﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace EarthsLastHope.Factory
{
    public abstract class EnemyShipFactory
    {
        public abstract Enemy CreateEnemy(Vector3 position);
    }
}
