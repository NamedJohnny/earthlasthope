﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace EarthsLastHope.Factory
{
    public class MothershipFactory : EnemyShipFactory
    {
        public override Enemy CreateEnemy(Vector3 position)
        {
            return new MotherShip(position);
        }
    }
}
