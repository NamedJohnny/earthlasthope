﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;
//using Microsoft.Xna.Framework;
//using Microsoft.Xna.Framework.Content;
//using Microsoft.Xna.Framework.Graphics;

//namespace EarthsLastHope
//{
//    public class City : GameObject
//    {


//        Building[,] batiment;
//        GameObject ocean;
//        GameObject island;
//        GameObject autoroute;
//        GameObject autoroute2;
//        GameObject[] parcs;

//        //Models//
//        Model[] batimentList = new Model[13];
//        Model modelParc;
//        /////////

//        int nbParcs;
//        int nbBuildingsDepth;
//        int nbBuildingsWide;
//        int spacingWide;
//        int spacingDepth;

//        public int Width
//        {
//            get { return spacingWide * nbBuildingsWide; }
//        }

//        public int Depth
//        {
//            get { return spacingDepth * nbBuildingsDepth; }
//        }
        
//        public City(Vector3 position, int nbParcs = 15, int nbBuildingsWide = 12, int nbBuildingDepth = 12, int spacingWide = 6000, int spacingDepth = 6000)
//            :base(position)
//        {
//            this.nbParcs = nbParcs;
//            this.nbBuildingsDepth = nbBuildingDepth;
//            this.nbBuildingsWide = nbBuildingsWide;
//            this.spacingWide = spacingWide;
//            this.spacingDepth = spacingDepth;

//            batiment = new Building[nbBuildingsWide, nbBuildingDepth];
//            parcs = new GameObject[nbParcs];
//            batimentList = new Model[13];

//        }


//        public void Initialise()
//        {
//            Random rand = new Random();
//            int parcX;
//            int parcY;
//            float xScale;
//            float yScale;
//            float zScale;

//            ocean = new Building(new Vector3(0, 0, 0), new Vector3(3500, 1, 3500));
//            island = new Building(new Vector3(0, 0, 0), new Vector3(500, 4, 500));
//            autoroute = new Building(new Vector3(0, 1000, 2720), new Vector3(2500, 2, 4));
//            autoroute2 = new Building(new Vector3(2720, 1500, 0), new Vector3(4, 2, 2500));

//            for (int i = 0; i < nbParcs; i++)
//            {
//                parcX = rand.Next(Width) - ((Width) / 2);
//                parcY = rand.Next(Depth) - ((Depth) / 2);
//                parcs[i] = new Building(new Vector3(parcX, 550, parcY), new Vector3(20, 1, 20));
//            }

//            for (int i = 0; i < nbBuildingsWide; i++)
//            {
//                for (int k = 0; k < nbBuildingsDepth; k++)
//                {
//                    int yFactor = rand.Next(2);

//                    xScale = rand.Next(25) + 10;
//                    yScale = rand.Next(60) + 10;
//                    if (yFactor == 1)
//                        yScale += 50;
//                    zScale = rand.Next(25) + 10;

//                    batiment[i, k] = new Building(new Vector3((i * spacingWide) - ((Width) / 2), 0, (k * spacingDepth) - ((Depth) / 2)), new Vector3(xScale, yScale, zScale));
//                }
//            }
//        }



//        public void LoadContent(ContentManager Content)
//        {
//            Random rand = new Random();

//            ocean.model = Content.Load<Model>("./Content/Models/Ville/cube");
//            ocean.texture = Content.Load<Texture2D>("./Content/Textures/city/_Water_Deep_1");

//            island.model = Content.Load<Model>("./Content/Models/Ville/cube");
//            island.texture = Content.Load<Texture2D>("./Content/Textures/city/Concrete_Block_8x8_Gray");

//            autoroute.model = Content.Load<Model>("./Content/Models/Ville/cube2");

//            autoroute2.model = Content.Load<Model>("./Content/Models/Ville/cube2");

//            modelParc = Content.Load<Model>("./Content/Models/Ville/cubePark");

//            for (int i = 0; i < nbParcs; i++)
//            {
//                parcs[i].model = modelParc;
//            }

//            batimentList[0] = Content.Load<Model>("./Content/Models/Ville/cube2");
//            batimentList[1] = Content.Load<Model>("./Content/Models/Ville/cube3");
//            batimentList[2] = Content.Load<Model>("./Content/Models/Ville/cube4");
//            batimentList[3] = Content.Load<Model>("./Content/Models/Ville/cube5");
//            batimentList[4] = Content.Load<Model>("./Content/Models/Ville/cube6");
//            batimentList[5] = Content.Load<Model>("./Content/Models/Ville/cube7");
//            batimentList[6] = Content.Load<Model>("./Content/Models/Ville/cube8");
//            batimentList[7] = Content.Load<Model>("./Content/Models/Ville/cube9");
//            batimentList[8] = Content.Load<Model>("./Content/Models/Ville/cube10");
//            batimentList[9] = Content.Load<Model>("./Content/Models/Ville/cube11");
//            batimentList[10] = Content.Load<Model>("./Content/Models/Ville/cube12");
//            batimentList[11] = Content.Load<Model>("./Content/Models/Ville/cube13");
//            batimentList[12] = Content.Load<Model>("./Content/Models/Ville/cube14");

//            for (int i = 0; i < nbBuildingsWide; i++)
//            {
//                for (int k = 0; k < nbBuildingsDepth; k++)
//                {
//                    int texture = rand.Next(12);
//                    batiment[i, k].model = batimentList[texture];
//                }
//            }
//        }

//        public override void Draw(Camera cam)
//        {
//            ocean.Draw(cam);
//            island.Draw(cam);
//            autoroute.Draw(cam);
//            autoroute2.Draw(cam);

//            for (int i = 0; i < nbParcs; i++)
//            {
//                parcs[i].Draw(cam);
//            } 

//            for (int i = 0; i < nbBuildingsWide; i++)
//            {
//                for (int k = 0; k < nbBuildingsDepth; k++)
//                {
//                    batiment[i, k].Draw(cam);
//                }
//            }
            
        
//        }

//    }
//}
