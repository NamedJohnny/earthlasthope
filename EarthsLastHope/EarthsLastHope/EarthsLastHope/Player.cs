﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace EarthsLastHope
{
    public class Player : GameObject
    {
        public Player(Vector3 position, Vector3 scale, Quaternion rotation)
            : base(position, scale, Quaternion.Identity) { }

        // TODO la classe Player car un joueur peut être autre chose qu'un Jet
    }
}
