﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace EarthsLastHope
{
    public class Bullet : GameObject
    {
        public static Texture2D texture;
        public static Effect effect;
        public static GraphicsDevice graphicsDevice;

        //public Vector3 position;
        //public Quaternion rotation;
        public Vector3 bulletback;
        public Vector3 camUp;

        //public Effect effect;
        //public Texture2D texture;

        public float moveSpeed = 200f;
        public float gravity = 0;

        public Bullet(Vector3 position, Quaternion rotation) : base(position, new Vector3()) {
            this.rotation = rotation;
        }

        public override void Update() {
            MoveForward();
            position.Y -= gravity;
            gravity += 0.1f;
        }

        public override void Draw(Camera cam)
        {
            VertexPositionTexture[] bulletVertices = new VertexPositionTexture[6];
            int i = 0;
            Vector3 center = position;

            bulletVertices[i++] = new VertexPositionTexture(center, new Vector2(1, 1));
            bulletVertices[i++] = new VertexPositionTexture(center, new Vector2(0, 0));
            bulletVertices[i++] = new VertexPositionTexture(center, new Vector2(1, 0));

            bulletVertices[i++] = new VertexPositionTexture(center, new Vector2(1, 1));
            bulletVertices[i++] = new VertexPositionTexture(center, new Vector2(0, 1));
            bulletVertices[i++] = new VertexPositionTexture(center, new Vector2(0, 0));


            effect.CurrentTechnique = effect.Techniques["PointSprites"];
            effect.Parameters["xWorld"].SetValue(Matrix.Identity);
            effect.Parameters["xProjection"].SetValue(cam.projectionMatrix);
            effect.Parameters["xView"].SetValue(cam.viewMatrix);
            effect.Parameters["xTexture"].SetValue(texture);
            effect.Parameters["xPointSpriteSize"].SetValue(30f);
            effect.Parameters["xCamPos"].SetValue(cam.position);
            effect.Parameters["xCamUp"].SetValue(bulletback);

            foreach (EffectPass pass in effect.CurrentTechnique.Passes)
            {
                pass.Apply();
                graphicsDevice.DrawUserPrimitives(PrimitiveType.TriangleList, bulletVertices, 0, 2);
            }
        }

        private void MoveForward()
        {
            Vector3 addVector = Vector3.Transform(new Vector3(0, 0, -1), rotation);
            position += addVector * moveSpeed;
        }
    }
}
