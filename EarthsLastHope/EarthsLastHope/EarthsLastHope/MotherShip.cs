﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace EarthsLastHope
{
    class MotherShip : Enemy
    {
        public MotherShip(Vector3 position)
            : base(position)
        {
            this.pointWorth = 10000;
            this.rayon = 10000;
            this.rotationSpeed = 5000;
            this.scale = new Vector3(20, 20, 20);
        }


    }
}
