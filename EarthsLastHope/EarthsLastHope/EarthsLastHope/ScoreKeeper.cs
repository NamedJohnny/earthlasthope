﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EarthsLastHope.Observer;

namespace EarthsLastHope
{
    public class ScoreKeeper : IObserver
    {
        private int score;

        public int Score
        {
            get { return score; }
            set { score = value; }
        }

        public void Update(int pointWorth)
        {
            score += pointWorth;
        }
    }
}
