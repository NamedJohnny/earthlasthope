﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using EarthsLastHope.AIStates;
using EarthsLastHope.Observer;
//using EarthsLastHope.AIStates;

namespace EarthsLastHope
{

    public abstract class Enemy : GameObject, ISubject
    {
        protected int angle;
        protected int rotationSpeed;
        protected int rayon;
        protected float anglePerpenduculaire; 
        
        public int viewRange = 20000;
        public int safetyRange = 10000;
         
        protected float speed = 0;
        protected const float MAXSPEED = 80f;
        protected const float ACCELERATION = 0.5f;

        protected float slerpAmount = 0.001f;

        protected AIState state;

        public List<Player> possibleTargets;

        protected int pointWorth;

        //We need a collection of observers to notify.
        List<IObserver> _observers = new List<IObserver>();

        public Enemy(Vector3 position)
            : this(position, new List<Player>())
        {           
            
        }

        public Enemy(Vector3 position, List<Player> possibleTargets)
            : base(position)
        {
            this.rotationSpeed = 1000;
            this.rayon = 5000;
            this.possibleTargets = possibleTargets;

            angle = 0;
            anglePerpenduculaire = (float)360 / rotationSpeed;

            state = new Idle(this);
        }

        public void MoveTo(Vector3 destination)
        {
            // TODO : 3D A* algorithm

            if (slerpAmount < 0.15f)
                slerpAmount += 0.0001f;

            rotation = Quaternion.Slerp(rotation, GetRotation(position, destination), slerpAmount);
            Accelerate();
        }

        public void Stop()
        {
            if (slerpAmount < 0.15f)
                slerpAmount += 0.0001f;

            SlowDown();
        }
        protected void Accelerate()
        {
            MoveForward(ACCELERATION);
        }
        protected void SlowDown()
        {
            MoveForward(-ACCELERATION);
        }
        private void MoveForward(float acc)
        {
            speed = MathHelper.Clamp(speed + acc, 0, MAXSPEED);

            Vector3 addVector = Vector3.Transform(new Vector3(0, 0, -1), rotation);
            position += addVector * speed;
        }


        protected Quaternion GetRotation(Vector3 origine, Vector3 destination)
        {
            Vector3 angle = destination - origine;
            angle.Normalize();

            Matrix laRotation = Matrix.CreateLookAt(Vector3.Zero, angle, Vector3.Up);
            laRotation = Matrix.Transpose(laRotation);
            return Quaternion.CreateFromRotationMatrix(laRotation);
        }

        public void ChangeState(AIState newState)
        {
            state = newState;
        }

        /// <summary>
        /// Update l'enemie selon son comportement associé et selon son environnement
        /// </summary>
        public override void Update()
        {
            state.Update();
        }

        public void AddTarget(Player player)
        {
            possibleTargets.Add(player);
        }
        public void RemoveTarget(Player player)
        {
            possibleTargets.Remove(player);
        }

        public bool IsMoving()
        {
            return speed > 0;
        }

        public void Kill()
        {
            //The observers are notified whenever the class's state, 
            //in this case the Player's hit points changes.
            NotifyObservers();
        }

        public void RegisterObserver(IObserver observer)
        {
            _observers.Add(observer);
        }

        public void UnregisterObserver(IObserver observer)
        {
            _observers.Remove(observer);
        }

        public void NotifyObservers()
        {
            //Here we simply iterate through each registered 
            //observer and pass each one
            //the current hitpoints.
            foreach (IObserver observer in _observers)
            {
                observer.Update(pointWorth);
            }
        }

    }
}
