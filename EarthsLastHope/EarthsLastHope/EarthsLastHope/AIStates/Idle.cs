﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EarthsLastHope.AIStates
{
    class Idle : AIState
    {
        public Idle(Enemy enemy)
            : base(enemy) { }

        /// <summary>
        /// Si un Player est suffisament proche, on le prends en chase
        /// Aussi, on ralentit s'il est trop loin
        /// </summary>
        public override void Update()
        {
            if (enemy.IsMoving())
                enemy.Stop();

            foreach (Player player in enemy.possibleTargets)
            {
                if (Util.GetDistance(enemy.position, player.position) < enemy.viewRange) {
                    enemy.ChangeState(new Chasing(enemy, player));
                    return;
                }
            }
        }
    }
}
