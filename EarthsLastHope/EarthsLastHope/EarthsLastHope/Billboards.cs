using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;


namespace EarthsLastHope
{
    class Billboards
    {

        static Random r = new Random();
        float positionx;
        float positiony;
        float positionz;

        int size;
        int posY = 1000000;
        int scale = 140000;
        public const int BILLBOARD_SPHERIQUE = 0;
        public const int BILLBOARD_CYLINDRIQUE = 1;
        public bool isActive;

        GraphicsDevice device;
        BasicEffect basicEffect;

        VertexPositionTexture[] billboardVertices;
        VertexDeclaration myVertexDeclaration;
        List<Vector4> billboardList = new List<Vector4>();
        int type;
        Texture2D texture;

        public Billboards()
        {

        }


        public void Initialise(GraphicsDevice LeDevice)
        {
            device = LeDevice;
            basicEffect = new BasicEffect(LeDevice);
            myVertexDeclaration = new VertexDeclaration(VertexPositionTexture.VertexDeclaration.GetVertexElements());
        }


        public void AddBillboard(int x, int y, int z, int echelle, int leType, Texture2D laTexture)
        {
            billboardList.Add(new Vector4(x, y, z, echelle));
            type = leType;
            texture = laTexture;
        }
        private void AddBillboardsSPHERIQUE(int nbBill)
        {
            for (int x = 0; x < nbBill; x++)
            {
                size = (int)r.Next(scale, scale * 3);
                positiony = r.Next(posY / 2, posY * 2);
                positionx = r.Next(10000000);
                positionz = r.Next(10000000);
                positionz = positionz - 5000000;
                positionx = positionx - 5000000;
                billboardList.Add(new Vector4(positionx, positiony, positionz, size));
            }
        }

        private void randomyseMe(int nb)
        {
            for (int i = 0; i < nb; i++)
            {
                r.Next(100);
            }
        }
        public void AddBillboard2(int leType, Texture2D laTexture, int nb, int nbBill, int echelle)
        {
            billboardList.Add(new Vector4(echelle));
            randomyseMe(nb);
            if (leType == BILLBOARD_SPHERIQUE)
            {
                AddBillboardsSPHERIQUE(nbBill);
            }
            type = leType;
            texture = laTexture;
        }
        public void Update(Vector3 CamPosition, Vector3 CamUp, Vector3 CamAvant)
        {
            CreateBBVertices(CamPosition, CamUp, CamAvant);
        }

        private void CreateBBVertices(Vector3 CamPosition, Vector3 CamUp, Vector3 CamAvant)
        {
            int i = 0;
            int j = 0;


            billboardVertices = new VertexPositionTexture[billboardList.Count * 6];

            foreach (Vector4 currentV4 in billboardList)
            {
                Vector3 center = new Vector3(currentV4.X, currentV4.Y, currentV4.Z);
                float scaling = currentV4.W;
                Matrix bbMatrix;

                if (type == BILLBOARD_SPHERIQUE)
                    bbMatrix = Matrix.CreateBillboard(center, CamPosition, CamUp, CamAvant);
                else
                    bbMatrix = Matrix.CreateConstrainedBillboard(center, CamPosition, new Vector3(0, 1, 0), CamAvant, null);

                //first triangle
                Vector3 posDL = new Vector3(-0.5f, -0.5f, 0);
                Vector3 billboardedPosDL = Vector3.Transform(posDL * scaling, bbMatrix);
                billboardVertices[i++] = new VertexPositionTexture(billboardedPosDL, new Vector2(1, 1));
                Vector3 posUR = new Vector3(0.5f, 0.5f, 0);
                Vector3 billboardedPosUR = Vector3.Transform(posUR * scaling, bbMatrix);
                billboardVertices[i++] = new VertexPositionTexture(billboardedPosUR, new Vector2(0, 0));
                Vector3 posUL = new Vector3(-0.5f, 0.5f, 0);
                Vector3 billboardedPosUL = Vector3.Transform(posUL * scaling, bbMatrix);
                billboardVertices[i++] = new VertexPositionTexture(billboardedPosUL, new Vector2(1, 0));

                //second triangle: 2 of 3 cornerpoints already calculated!
                billboardVertices[i++] = new VertexPositionTexture(billboardedPosDL, new Vector2(1, 1));
                Vector3 posDR = new Vector3(0.5f, -0.5f, 0);
                Vector3 billboardedPosDR = Vector3.Transform(posDR * scaling, bbMatrix);
                billboardVertices[i++] = new VertexPositionTexture(billboardedPosDR, new Vector2(0, 1));
                billboardVertices[i++] = new VertexPositionTexture(billboardedPosUR, new Vector2(0, 0));
                j++;
            }
        }

        public void Draw(Matrix ViewMatrix, Matrix ProjectionMatrix, bool applyFog)
        {
            //draw billboards
            basicEffect.World = Matrix.Identity;
            basicEffect.View = ViewMatrix;
            basicEffect.Projection = ProjectionMatrix;
            basicEffect.TextureEnabled = true;
            basicEffect.Texture = texture;
            basicEffect.Alpha = 1;
            
            if (applyFog){
                basicEffect.FogEnabled = true;
                basicEffect.FogEnd = 50000;
                basicEffect.FogColor = new Vector3(1.0f, 1.0f, 1.0f);
            }

            basicEffect.CurrentTechnique.Passes[0].Apply();
            foreach (EffectPass pass in basicEffect.CurrentTechnique.Passes)
            {
                if (isActive)
                {
                    pass.Apply();

                    device.DrawUserPrimitives<VertexPositionTexture>(PrimitiveType.TriangleList, billboardVertices, 0, billboardList.Count * 2);
                }
            }
        }
    }
}
