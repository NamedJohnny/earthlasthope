using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;


namespace EarthsLastHope
{

    public class Game1 : Microsoft.Xna.Framework.Game
    {
        GraphicsDeviceManager graphics;
        ScreenManager screenManager;

        static int SCREEN_WIDTH = 1440;
        static int SCREEN_HEIGHT = 900;

        static readonly string[] preloadAssets =
        {
            "./Content/gradient",
        };

        public Game1()
        {
            Content.RootDirectory = "Content";
            graphics = new GraphicsDeviceManager(this);
            SetupGraphicsDevice();

            screenManager = new ScreenManager(this);

            Components.Add(screenManager);

            screenManager.AddScreen(new BackgroundScreen(), null);
            screenManager.AddScreen(new MainMenuScreen(), null);
        }

        void SetupGraphicsDevice()
        {
            graphics.PreferMultiSampling = true; //anti-aliasing
            graphics.PreferredBackBufferWidth = SCREEN_WIDTH; //width
            graphics.PreferredBackBufferHeight = SCREEN_HEIGHT; //height
            graphics.IsFullScreen = false;   //fullscreen
            graphics.ApplyChanges();
            Window.Title = "Earth's Last Hope";
        }

        protected override void Initialize()
        {
            base.Initialize();
        }

        protected override void LoadContent()
        {
            foreach (string asset in preloadAssets)
            {
                Content.Load<object>(asset);
            }
        }

        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.Black);

            base.Draw(gameTime);
        }

        /*static class Program
        {
            static void Main()
            {
                using (Game1 game = new Game1())
                {
                    game.Run();
                }
            }
        }*/
    }
}
