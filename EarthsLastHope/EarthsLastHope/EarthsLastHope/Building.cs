﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace EarthsLastHope
{
    class Building : GameObject
    {
        public Building(Vector3 position, Vector3 scale)
            : base(position, scale)
        {

        }

        public override void Draw(Camera cam)
        {
            Matrix worldMatrix = Matrix.CreateScale(scale) * Matrix.CreateRotationY(MathHelper.Pi) * Matrix.CreateTranslation(position);

            Matrix[] xwingTransforms = new Matrix[model.Bones.Count];
            model.CopyAbsoluteBoneTransformsTo(xwingTransforms);

            base.Draw(cam);
        }
    }
}
