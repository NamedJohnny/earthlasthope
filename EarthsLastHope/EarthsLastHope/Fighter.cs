﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace EarthsLastHope
{
    class Fighter : Enemy
    {
        public Fighter(Vector3 position)
            : this(position, new List<Player>())
        {
        }

        public Fighter(Vector3 position, List<Player> possibleTargets)
            : base(position, possibleTargets)
        {
            this.pointWorth = 150;
            this.rayon = 5000;
            this.rotationSpeed = 1000;

            this.scale = new Vector3(1, 1, 1);
        
        }

        /*public void Patrol(Vector3 patrolLocation)
        {
            position.X = (float)(patrolLocation.X + (rayon * Math.Cos(angle * 2 * MathHelper.Pi / rotationSpeed)));
            position.Z = (float)(patrolLocation.Z + (rayon * Math.Sin(angle * 2 * MathHelper.Pi / rotationSpeed)));
            //rotation.Y -= MathHelper.ToRadians(anglePerpenduculaire);
            angle++;
        }*/

        public override void Draw(Camera cam)
        {
            Matrix worldMatrix = Matrix.CreateScale(scale) * Matrix.CreateRotationY(MathHelper.Pi) * Matrix.CreateFromQuaternion(rotation) * Matrix.CreateTranslation(position);

            Matrix[] transform = new Matrix[model.Bones.Count];
            model.CopyAbsoluteBoneTransformsTo(transform);
            foreach (ModelMesh mesh in model.Meshes)
            {
                foreach (BasicEffect effect in mesh.Effects)
                {
                    effect.EnableDefaultLighting();

                    effect.World = transform[mesh.ParentBone.Index] * worldMatrix;
                    effect.Projection = cam.projectionMatrix;
                    effect.View = cam.viewMatrix;
                }
                mesh.Draw();
            }
        }
    }
}
