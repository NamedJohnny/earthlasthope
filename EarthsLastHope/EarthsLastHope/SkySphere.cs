﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace EarthsLastHope
{
    class SkySphere : GameObject
    {
        protected GameObject target;

        protected Vector3 primitiveRotation = Vector3.Zero;
        protected float rotationSpeed;

        public SkySphere(Vector3 position, Vector3 scale, GameObject target, float rotationSpeed)
            : base(position, scale)
        {
            this.target = target;
            this.rotationSpeed = rotationSpeed;
        }

        public override void Update()
        {
            position = target.position;

            primitiveRotation.Y += MathHelper.ToRadians(rotationSpeed);
            if (primitiveRotation.Y == 360)
            {
                primitiveRotation.Y = 0;
            }
        }

        public override void Draw(Camera cam)
        {
            foreach (ModelMesh mesh in model.Meshes)
            {
                foreach (BasicEffect effect in mesh.Effects)
                {
                    if (texture != null)
                    {
                        effect.TextureEnabled = true;
                        effect.Texture = texture;
                    }

                    effect.PreferPerPixelLighting = false;
                    effect.World = Matrix.CreateFromYawPitchRoll(rotation.Y, rotation.X, rotation.Z) * Matrix.CreateScale(scale) * Matrix.CreateTranslation(position);
                    effect.Projection = cam.projectionMatrix;
                    effect.View = cam.viewMatrix;

                    effect.DirectionalLight0.Enabled = false;
                    effect.LightingEnabled = false;
                    effect.EmissiveColor = new Vector3(1.0f, 1.0f, 1.0f);
                    effect.FogEnabled = false;
                } mesh.Draw();
            }
        }
    }
}
