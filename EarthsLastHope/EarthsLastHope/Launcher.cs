﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;

namespace EarthsLastHope
{

    public class Launcher
    {
        public static int NB_MAX_MISSILES = 4;
        public struct MissileJet
        {
            public Missile missile;
            public float positionOnJet;
        }
        public float[] missileInitPos = new float[4];
        MissileJet[] missileTab = new MissileJet[4];
        public double lastMissileTime = -1000000000;
        public Model missileModel;
        public Texture2D smokePuff;

        Effect effect;
        ParticleSystem particle;
        private Vector3 scale;
        Vector3 position;
        Quaternion rotation;
        Vector3 jetRight;

        public Launcher(Vector3 scale, ParticleSystem particle)
        {
            this.scale = scale;
            this.particle = particle;

            missileInitPos[0] = -9;
            missileInitPos[1] = 9;
            missileInitPos[2] = -7;
            missileInitPos[3] = 7;
        }

        public void ReloadMissiles()
        {
            int b = missileTab.Count(x => x.missile != null && !x.missile.isFired);

            for (int i = 0; i < Launcher.NB_MAX_MISSILES - b; i++)
            {
                missileTab[i].missile = (new Missile(missileModel, scale, particle));
                missileTab[i].positionOnJet = missileInitPos[i];
            }
        }

        public void LoadContent(ContentManager content)
        {
            SoundManager.AddSound("launcher.missile1", content.Load<SoundEffect>("./Content/Audio/missile1"));
            SoundManager.AddSound("launcher.missile2", content.Load<SoundEffect>("./Content/Audio/missile2"));
            missileModel = content.Load<Model>("./Content/Models/jetMissile");
            smokePuff = content.Load<Texture2D>("Content/Textures/Billboards/smoke");
        }

        public void Update(GameTime gameTime, Vector3 position, Quaternion rotation, Vector3 jetRight)
        {
            this.position = position;
            this.rotation = rotation;
            this.jetRight = jetRight;
            UpdateMissiles(gameTime);
        }

        private void UpdateMissiles(GameTime gameTime)
        {
            var listMissile = missileTab.ToList().FindAll(x => x.missile != null);

            for (int i = 0; i < listMissile.Count; i += 1) //  for each file
            {
                MissileJet missileJet = listMissile[i];
                Vector3 position = this.position + (jetRight * missileJet.positionOnJet);
                missileJet.missile.Update(gameTime, position, rotation);
                if (!missileJet.missile.isActive)
                    missileJet.missile = null;
            }
        }

        /// <summary>
        /// Dessine les missiles
        /// </summary>
        public void DrawMissiles(Camera cam)
        {
            var listMissile = missileTab.ToList().FindAll(x => x.missile != null);

            foreach (MissileJet missileJet in listMissile)
            {
                missileJet.missile.Draw(cam);
            }
        }

        public void FireMissile(GameTime gameTime, float speed, Vector3 target)
        {
            double currentTime = gameTime.TotalGameTime.TotalMilliseconds;

            if (missileTab.Count(x => x.missile != null) > 0)
            {

                if (currentTime - lastMissileTime > 500)
                {
                    MissileJet missileJet = missileTab.FirstOrDefault(x => x.missile != null && !x.missile.isFired && x.missile.isActive);

                    if (missileJet.missile != null)
                    {
                        Random rand = new Random();
                        if (rand.Next(1) == 0)
                            SoundManager.GetSound("launcher.missile1").Play();
                        else
                            SoundManager.GetSound("launcher.missile2").Play();


                        missileJet.missile.Fire(target, speed);

                        lastMissileTime = currentTime;
                    }
                }
            }
        }


        internal int getMissilesCount()
        {
            return missileTab.Count(x => x.missile != null && !x.missile.isFired);
        }

        public MissileJet[] MissileTab
        {
            get
            {
                return missileTab.Where(x => x.missile != null).ToArray();
            }
        }

    }
}
