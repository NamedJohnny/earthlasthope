﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EarthsLastHope.AIStates
{
    class Chasing : AIState
    {
        protected GameObject target;

        public Chasing(Enemy enemy, GameObject target) : base(enemy)
        {
            this.target = target;
        }

        public override void Update()
        {
            if (Util.GetDistance(enemy.position, target.position) > enemy.safetyRange)
                enemy.MoveTo(target.position);
            else
                enemy.ChangeState(new Idle(enemy));
            // TODO : pew pew!!! bang!
        }
    }
}
