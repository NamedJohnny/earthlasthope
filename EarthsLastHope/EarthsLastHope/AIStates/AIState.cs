﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EarthsLastHope.AIStates
{
   public abstract class AIState
    {
        protected Enemy enemy;

        public AIState(Enemy enemy)
        {
            this.enemy = enemy;
        }

        public abstract void Update();
    }
}
