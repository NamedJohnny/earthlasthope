﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace EarthsLastHope.AIStates
{
    class Patrol : AIState
    {
        protected Vector3 patrolLocation;

        public Patrol(Enemy enemy, Vector3 patrolLocation) : base(enemy)
        {
            this.patrolLocation = patrolLocation;
        }

        public override void Update()
        {
            // TODO movement de patrol (tourne en rond)
            foreach (Player player in enemy.possibleTargets)
            {
                if (Util.GetDistance(enemy.position, player.position) < enemy.viewRange)
                {
                    enemy.ChangeState(new Chasing(enemy, player));
                    return;
                }
            }
        }
    }
}
