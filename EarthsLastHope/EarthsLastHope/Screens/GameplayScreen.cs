#region File Description
//-----------------------------------------------------------------------------
// GameplayScreen.cs
//
// Microsoft XNA Community Game Platform
// Copyright (C) Microsoft Corporation. All rights reserved.
//-----------------------------------------------------------------------------
#endregion

#region Using Statements
using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using EarthsLastHope.Factory;
#endregion

namespace EarthsLastHope
{
    class GameplayScreen : GameScreen
    {
        #region Instancation

        float pauseAlpha;

        SpriteBatch spriteBatch;
        ContentManager Content;

        //Variables statics//
        static int SCREEN_WIDTH = 1440;
        static int SCREEN_HEIGHT = 900;
        static int NB_FIGHTER = 8;
        static int NB_FUMEES = 30;
        static int NB_PARC = 15;
        static int NB_BUILDINGX = 12; //10
        static int NB_BUILDINGZ = 12; //10
        static int SPACE_IN_X = 6000; //5500
        static int SPACE_IN_Z = 6000; //5500
        static int NB_BUILDING = NB_BUILDINGX * NB_BUILDINGZ;
        static int CUBE_COLLISION_SCALE = 150;
        static int ARTILLERY_RATE = 5;

        static int MAP_SIZE_X = SPACE_IN_X * NB_BUILDINGX;
        static int MAP_SIZE_Z = SPACE_IN_Z * NB_BUILDINGZ;
        /////////////////////

        //Framerate//
        int frameRate = 0;
        int frameCounter = 0;
        TimeSpan elapsedTime = TimeSpan.Zero;
        /////////////

        //GameObjects//
        Jet jet;
        SkySphere skySphere;
        MotherShip motherShip;
        AmmoBox parachute = new AmmoBox(new Vector3(0, 10000, 0), new Vector3(1, 1, 1));
        Fighter[] fighter = new Fighter[NB_FIGHTER];
        Building[,] building = new Building[NB_BUILDINGX, NB_BUILDINGZ];
        GameObject ocean;
        GameObject island;
        GameObject highway;
        GameObject highway2;
        GameObject[] parc = new GameObject[NB_PARC];
        ///////////////

        //Models//
        Model[] buildingList = new Model[13];
        Model modelParc;
        /////////

        //Camera//
        Camera cam;
        //////////

        //Billboards//
        Billboards billboardsSmoke;
        Billboards billboardsSun;
        //////////////

        //Explosions//
        ParticleSystem explosionParticles;
        ParticleSystem explosionSmokeParticles;
        ParticleSystem projectileTrailParticles;
        //////////////

        //Interface//
        Texture2D HUD;
        Texture2D BlackFade;
        Rectangle FullScreenRectangle;
        Texture2D cursorAlt;
        Rectangle cursorRectangleAlt;
        Texture2D cursorSpeed;
        Rectangle cursorRectangleSpeed;
        Texture2D healthBar;
        Rectangle healthBarRectangle;
        Texture2D healthBarBase;
        Rectangle healthBarBaseRectangle;
        Texture2D grayCursor;
        Rectangle[] grayRectangleCursor = new Rectangle[NB_FIGHTER];
        Texture2D blueCursor;
        Rectangle blueRectangleCursor;
        Texture2D redCursor;
        Rectangle redRectangleCursor;
        Texture2D crossair;
        Rectangle rectangleCrossair;
        Color interfaceColor;
        /////////////

        //Collisions//
        BoundingFrustum viewFrustum;
        BoundingSphere BSJet;
        BoundingBox[,] BBCube = new BoundingBox[NB_BUILDINGX, NB_BUILDINGZ];
        BoundingSphere[] BSFighter = new BoundingSphere[NB_FIGHTER];
        BoundingSphere BSParachute;
        BoundingBox BBFloor;
        BoundingBox BBOcean;
        BoundingBox BBHighway;
        BoundingBox BBHighway2;
        BoundingSphere BSMissilesBorders;
        //////////////

        //Audio//
        //SoundEffect jetEngine;
        SoundEffectInstance jetEngineInstance;
        //SoundEffect jetEngine2;
        SoundEffectInstance jetEngineInstance2;
        //SoundEffect staticRadio;
        SoundEffectInstance staticRadioInstance;
        //SoundEffect reloadSound;
        //SoundEffect explosionSound;
        /////////

        //Ambiance//
        int artillerySteps = 0;
        ////////////

        //Fonts//
        SpriteFont interfaceFont;
        SpriteFont distanceInterfaceFont;
        Color couleurDistanceInterface;
        SpriteFont visitorFont;
        /////////

        ScoreKeeper scoreKeeper;

        #endregion

        #region Initialisation
        public GameplayScreen()
        {
            TransitionOnTime = TimeSpan.FromSeconds(1.5);
            TransitionOffTime = TimeSpan.FromSeconds(0.5);

            InitInterface();

            InitVille();

            InitBillboards();

            scoreKeeper = new ScoreKeeper();
        }

        #endregion

        #region Load
        public override void LoadContent()
        {
            if (Content == null)
                Content = new ContentManager(ScreenManager.Game.Services, "Content");

            ScreenManager.Game.ResetElapsedTime();

            #region Explosion
            explosionParticles = new ExplosionParticleSystem(ScreenManager.Game, Content);
            explosionSmokeParticles = new ExplosionSmokeParticleSystem(ScreenManager.Game, Content);
            projectileTrailParticles = new ProjectileTrailParticleSystem(ScreenManager.Game, Content);
            explosionParticles.DrawOrder = 10;
            explosionSmokeParticles.DrawOrder = 5;
            projectileTrailParticles.DrawOrder = 1;
            ScreenManager.Game.Components.Add(explosionParticles);
            ScreenManager.Game.Components.Add(explosionSmokeParticles);
            ScreenManager.Game.Components.Add(projectileTrailParticles);
            #endregion

            EnemyShipFactory enemyShipfactory = new MothershipFactory();
            motherShip = (MotherShip)enemyShipfactory.CreateEnemy(new Vector3(0, 25000, 0));

            jet = new Jet(new Vector3(0, 24000, 50000), new Vector3(2.5f, 2.5f, 2.5f), projectileTrailParticles);

            skySphere = new SkySphere(new Vector3(0, 0, 0), new Vector3(5000, 5000, 5000), jet, 0.03f);
            motherShip.rotation.Y = MathHelper.ToRadians(-90);
            skySphere.rotation.X = MathHelper.ToRadians(180);

            //cam = new Camera(jet, new Matrix(), Matrix.CreatePerspectiveFieldOfView(MathHelper.PiOver4, ScreenManager.GraphicsDevice.Viewport.AspectRatio, 1f, 1000000.0f));
            cam = new Camera(jet, new Vector3(0, 25, 100), new Matrix(), Matrix.CreatePerspectiveFieldOfView(MathHelper.PiOver4, ScreenManager.GraphicsDevice.Viewport.AspectRatio, 1f, 1000000.0f));
            //cam = new Camera(fighter[3], new Vector3(0, 1000, 1000), new Matrix(), Matrix.CreatePerspectiveFieldOfView(MathHelper.PiOver4, ScreenManager.GraphicsDevice.Viewport.AspectRatio, 1f, 1000000.0f));

            enemyShipfactory = new FighterFactory();

            for (int i = 0; i < NB_FIGHTER; i++)
            {
                fighter[i] = (Fighter)enemyShipfactory.CreateEnemy(new Vector3(-2500 + (i * 500), 12000, 10000));
                fighter[i].possibleTargets = new List<Player>() { jet };
                fighter[i].RegisterObserver(scoreKeeper);
            }

            viewFrustum = new BoundingFrustum(cam.viewMatrix * cam.projectionMatrix); // pr�pare le bounding frustum

            spriteBatch = new SpriteBatch(ScreenManager.GraphicsDevice);

            #region loadJet
            Bullet.texture = Content.Load<Texture2D>("./Content/Textures/Bullet");
            Bullet.effect = Content.Load<Effect>("./Content/effects");
            Bullet.graphicsDevice = ScreenManager.GraphicsDevice;

            jet.model = Content.Load<Model>("./Content/Models/jet");
            //jet.bulletTexture = Content.Load<Texture2D>("./Content/Textures/Bullet");
            jet.muzzleFlash = Content.Load<Texture2D>("./Content/Textures/MuzzleFlash");
            //jet.effect = Content.Load<Effect>("./Content/effects");

            SoundManager.AddSound("jet.gun", Content.Load<SoundEffect>("./Content/Audio/gun"));
            SoundManager.AddSound("jet.missile1", Content.Load<SoundEffect>("./Content/Audio/missile1"));
            SoundManager.AddSound("jet.missile2", Content.Load<SoundEffect>("./Content/Audio/missile2"));


            #endregion

            jet.smokePuff = Content.Load<Texture2D>("./Content/Textures/Billboards/smoke");
            skySphere.model = Content.Load<Model>("./Content/Models/SkySphere");
            skySphere.texture = Content.Load<Texture2D>("./Content/Textures/Sky02(3)");

            motherShip.model = Content.Load<Model>("./Content/Models/Hammerhead");

            parachute.model = Content.Load<Model>("./Content/Models/Parachute");

            for (int i = 0; i < NB_FIGHTER; i++)
                fighter[i].model = Content.Load<Model>("./Content/Models/fighter");

            jet.launcher.LoadContent(Content);

            jet.Initialize();

            LoadInterface();

            LoadVille();

            LoadBillboards();

            InitCollision(); // doit �tre initialis� ici, car les mod�les sont n�cessaires pour l'ajustement d'�l�ments de collision

            interfaceFont = Content.Load<SpriteFont>("./Content/Fonts/interfaceFont");
            distanceInterfaceFont = Content.Load<SpriteFont>("./Content/Fonts/distanceInterfaceFont");
            visitorFont = Content.Load<SpriteFont>("./Content/Fonts/SpriteFont1");



            SoundManager.AddSound("jet.engine", Content.Load<SoundEffect>("./Content/Audio/jetEngineProjet"));
            SoundManager.AddSound("jet.engine2", Content.Load<SoundEffect>("./Content/Audio/jetEngineProjet"));
            jetEngineInstance = SoundManager.GetSound("jet.engine").CreateInstance();
            jetEngineInstance2 = SoundManager.GetSound("jet.engine2").CreateInstance();

            SoundManager.AddSound("jet.staticRadio", Content.Load<SoundEffect>("./Content/Audio/staticRadio"));
            staticRadioInstance = SoundManager.GetSound("jet.staticRadio").CreateInstance();

            SoundManager.AddSound("explosion", Content.Load<SoundEffect>("./Content/Audio/explosion"));
            SoundManager.AddSound("jet.reload", Content.Load<SoundEffect>("./Content/Audio/reload"));
        }
        #endregion

        #region Unload
        public override void UnloadContent()
        {
            SoundManager.Clear();
            Content.Unload();
        }
        #endregion


        #region Update
        public override void Update(GameTime gameTime, bool otherScreenHasFocus,
                                                       bool coveredByOtherScreen)
        {
            base.Update(gameTime, otherScreenHasFocus, false);

            // Gradually fade in or out depending on whether we are covered by the pause screen.
            if (coveredByOtherScreen)
                pauseAlpha = Math.Min(pauseAlpha + 1f / 32, 1);
            else
                pauseAlpha = Math.Max(pauseAlpha - 1f / 32, 0);

            if (IsActive)
            {
                viewFrustum = new BoundingFrustum(cam.viewMatrix * cam.projectionMatrix); // update le bounding frustum

                /*scanForCollision();

                //motherShip.Patrol(Vector3.Zero);

                // Update les enemies
                motherShip.Update();
                foreach (Fighter f in fighter)
                {
                    f.Update();
                }*/

                /*fighter[0].Patrol(new Vector3(motherShip.position.X - 500, motherShip.position.Y, motherShip.position.Z - 500));
                fighter[1].Patrol(motherShip.position);
                fighter[2].Patrol(new Vector3(motherShip.position.X + 500, motherShip.position.Y, motherShip.position.Z + 500));
                fighter[3].Follow(jet);*/
                parachute.Update();
                skySphere.Update();

                billboardsSmoke.Update(cam.position, Vector3.Up, new Vector3(0, 0, 0));

                FramerateFind(gameTime);

                cursorRectangleAlt.Y = ((jet.altitude) * -1) + 640;
                cursorRectangleSpeed.Y = (((int)jet.moveSpeed) * -4) + 640;

                UpdateInterface();

                jet.Update(gameTime);

                cam.Update();

                ////
                ScanForCollision();

                //motherShip.Patrol(Vector3.Zero);

                // Update les enemies
                motherShip.Update();
                foreach (Fighter f in fighter)
                {
                    f.Update();
                }
                ///////

                UpdateBillboards();

                jetEngineInstance.Play();
                jetEngineInstance.Volume = 0.05f;
                jetEngineInstance.Pitch = ((jet.moveSpeed / Jet.MAX_SPEED) - 1);
                jetEngineInstance2.Play();
                jetEngineInstance2.Volume = (jet.moveSpeed / Jet.MAX_SPEED) / 2;
                jetEngineInstance2.Pitch = ((jet.moveSpeed / Jet.MAX_SPEED) - 1);
                staticRadioInstance.Play();
                staticRadioInstance.Volume = 1f;
            }
        }
        #endregion

        #region Draw
        public override void Init()
        {
            ScreenManager.GraphicsDevice.Clear(ClearOptions.Target, Color.Black, 0, 0);
            frameCounter++; // ne pas mettre dans le Update
        }

        public override void Draw3D()
        {
            ScreenManager.GraphicsDevice.BlendState = BlendState.Opaque;
            ScreenManager.GraphicsDevice.DepthStencilState = DepthStencilState.Default;
            ScreenManager.GraphicsDevice.SamplerStates[0] = SamplerState.LinearWrap;
            if (!jet.isExploding)
                jet.Draw(cam);
            skySphere.Draw(cam);
            motherShip.Draw(cam);
            parachute.Draw(cam);
            for (int i = 0; i < NB_FIGHTER; i++)
                fighter[i].Draw(cam);

            drawVille();
        }

        public override void Draw2D()
        {
            ScreenManager.GraphicsDevice.BlendState = BlendState.AlphaBlend; //assure la transparence
            ScreenManager.GraphicsDevice.DepthStencilState = DepthStencilState.DepthRead; // permet d'affich� plusieurs billboards vis-�-vis (profondeur)
            DrawExplosions(cam.viewMatrix, cam.projectionMatrix); //doit rester l�!!
            this.spriteBatch.Begin();
            DrawInterface();
            DrawBillboards();
            this.spriteBatch.End();
        }

        public override void Fader()
        {
            if (TransitionPosition > 0 || pauseAlpha > 0)
            {
                float alpha = MathHelper.Lerp(1f - TransitionAlpha, 1f, pauseAlpha / 2);

                ScreenManager.FadeBackBufferToBlack(alpha);
            }
        }
        #endregion

        public override void HandleInput(InputState input)
        {
            if (input == null)
                throw new ArgumentNullException("input");

            // Look up inputs for the active player profile.
            int playerIndex = (int)ControllingPlayer.Value;

            KeyboardState keyboardState = input.CurrentKeyboardStates[playerIndex];
            GamePadState gamePadState = input.CurrentGamePadStates[playerIndex];

            // The game pauses either if the user presses the pause button, or if
            // they unplug the active gamepad. This requires us to keep track of
            // whether a gamepad was ever plugged in, because we don't want to pause
            // on PC if they are playing with a keyboard and have no gamepad at all!
            bool gamePadDisconnected = !gamePadState.IsConnected &&
                                       input.GamePadWasConnected[playerIndex];

            if (input.IsPauseGame(ControllingPlayer) || gamePadDisconnected)
            {
                ScreenManager.AddScreen(new PauseMenuScreen(), ControllingPlayer);
            }
        }


        void FramerateFind(GameTime gameTime)
        {
            elapsedTime += gameTime.ElapsedGameTime;

            if (elapsedTime > TimeSpan.FromSeconds(1))
            {
                elapsedTime -= TimeSpan.FromSeconds(1);
                frameRate = frameCounter;
                frameCounter = 0;
            }

        }

        #region Ville
        void InitVille()
        {
            Random rand = new Random();
            int parcX;
            int parcY;
            float xScale;
            float yScale;
            float zScale;

            ocean = new Building(new Vector3(0, 0, 0), new Vector3(3500, 1, 3500));
            island = new Building(new Vector3(0, 0, 0), new Vector3(500, 4, 500));
            highway = new Building(new Vector3(0, 1000, 2720), new Vector3(2500, 2, 4));
            highway2 = new Building(new Vector3(2720, 1500, 0), new Vector3(4, 2, 2500));

            for (int i = 0; i < NB_PARC; i++)
            {
                parcX = rand.Next(MAP_SIZE_X) - ((MAP_SIZE_X) / 2);
                parcY = rand.Next(MAP_SIZE_Z) - ((MAP_SIZE_Z) / 2);
                parc[i] = new Building(new Vector3(parcX, 550, parcY), new Vector3(20, 1, 20));
            }

            for (int i = 0; i < NB_BUILDINGX; i++)
            {
                for (int k = 0; k < NB_BUILDINGZ; k++)
                {
                    int yFactor = rand.Next(2);

                    xScale = rand.Next(25) + 10;
                    yScale = rand.Next(60) + 10;
                    if (yFactor == 1)
                        yScale += 50;
                    zScale = rand.Next(25) + 10;

                    building[i, k] = new Building(new Vector3((i * SPACE_IN_X) - ((MAP_SIZE_X) / 2), 0, (k * SPACE_IN_Z) - ((MAP_SIZE_Z) / 2)), new Vector3(xScale, yScale, zScale));
                }
            }
        }

        void LoadVille()
        {
            Random rand = new Random();

            ocean.model = Content.Load<Model>("./Content/Models/Ville/cube");
            ocean.texture = Content.Load<Texture2D>("./Content/Textures/city/_Water_Deep_1");

            island.model = Content.Load<Model>("./Content/Models/Ville/cube");
            island.texture = Content.Load<Texture2D>("./Content/Textures/city/Concrete_Block_8x8_Gray");

            highway.model = Content.Load<Model>("./Content/Models/Ville/cube2");

            highway2.model = Content.Load<Model>("./Content/Models/Ville/cube2");

            modelParc = Content.Load<Model>("./Content/Models/Ville/cubePark");

            for (int i = 0; i < NB_PARC; i++)
            {
                parc[i].model = modelParc;
            }

            buildingList[0] = Content.Load<Model>("./Content/Models/Ville/cube2");
            buildingList[1] = Content.Load<Model>("./Content/Models/Ville/cube3");
            buildingList[2] = Content.Load<Model>("./Content/Models/Ville/cube4");
            buildingList[3] = Content.Load<Model>("./Content/Models/Ville/cube5");
            buildingList[4] = Content.Load<Model>("./Content/Models/Ville/cube6");
            buildingList[5] = Content.Load<Model>("./Content/Models/Ville/cube7");
            buildingList[6] = Content.Load<Model>("./Content/Models/Ville/cube8");
            buildingList[7] = Content.Load<Model>("./Content/Models/Ville/cube9");
            buildingList[8] = Content.Load<Model>("./Content/Models/Ville/cube10");
            buildingList[9] = Content.Load<Model>("./Content/Models/Ville/cube11");
            buildingList[10] = Content.Load<Model>("./Content/Models/Ville/cube12");
            buildingList[11] = Content.Load<Model>("./Content/Models/Ville/cube13");
            buildingList[12] = Content.Load<Model>("./Content/Models/Ville/cube14");

            for (int i = 0; i < NB_BUILDINGX; i++)
            {
                for (int k = 0; k < NB_BUILDINGZ; k++)
                {
                    int texture = rand.Next(12);
                    building[i, k].model = buildingList[texture];
                }
            }
        }

        void drawVille()
        {
            ocean.Draw(cam);
            island.Draw(cam);
            highway.Draw(cam);
            highway2.Draw(cam);

            for (int i = 0; i < NB_PARC; i++)
            {
                parc[i].Draw(cam);
            }

            for (int i = 0; i < NB_BUILDINGX; i++)
            {
                for (int k = 0; k < NB_BUILDINGZ; k++)
                {
                    building[i, k].Draw(cam);
                }
            }
        }
        #endregion

        #region Billboards
        void InitBillboards()
        {
            billboardsSmoke = new Billboards();
            billboardsSun = new Billboards();
        }

        void LoadBillboards()
        {
            Random rand = new Random();
            int posX;
            int posZ;
            int scale;

            billboardsSmoke.Initialise(ScreenManager.GraphicsDevice);
            billboardsSun.Initialise(ScreenManager.GraphicsDevice);

            Texture2D Fumee = Content.Load<Texture2D>("./Content/Textures/Billboards/Boucane");
            Texture2D Soleil = Content.Load<Texture2D>("./Content/Textures/Billboards/Soleil");
            Texture2D SmokePuff = Content.Load<Texture2D>("./Content/Textures/Billboards/smoke");

            for (int i = 0; i < NB_FUMEES; i++)
            {
                posX = rand.Next(SPACE_IN_X * NB_BUILDINGX) - ((SPACE_IN_X * NB_BUILDINGX) / 2);
                posZ = rand.Next(SPACE_IN_X * NB_BUILDINGX) - ((SPACE_IN_X * NB_BUILDINGX) / 2);
                scale = rand.Next(15000) + 15000;
                billboardsSmoke.AddBillboard(posX, scale / 2, posZ, scale, Billboards.BILLBOARD_CYLINDRIQUE, Fumee);
                billboardsSmoke.isActive = true;
            }
            billboardsSun.AddBillboard(-50000, 30000, -50000, 10000, Billboards.BILLBOARD_SPHERIQUE, Soleil);
            billboardsSun.isActive = true;

            billboardsSmoke.Update(cam.position, Vector3.Up, new Vector3(0, 0, 0));
            billboardsSun.Update(cam.position, Vector3.Up, new Vector3(0, 0, 0));
        }

        void DrawBillboards()
        {
            billboardsSmoke.Draw(cam.viewMatrix, cam.projectionMatrix, true);
            billboardsSun.Draw(cam.viewMatrix, cam.projectionMatrix, false);
        }

        void UpdateBillboards()
        {
            billboardsSmoke.Update(cam.position, Vector3.Up, new Vector3(0, 0, 0));
            billboardsSun.Update(cam.position, Vector3.Up, new Vector3(0, 0, 0));
        }
        #endregion

        #region Collision

        void InitCollision()
        {
            BSJet = jet.model.Meshes[0].BoundingSphere;
            BSJet.Radius *= jet.scale.X * 4;

            BSParachute = parachute.model.Meshes[0].BoundingSphere;
            BSParachute.Radius *= parachute.scale.X * 5;

            BSMissilesBorders = skySphere.model.Meshes[0].BoundingSphere;
            BSMissilesBorders.Radius = skySphere.scale.X;

            BBFloor = new BoundingBox(Vector3.Zero - new Vector3(island.scale.X * (CUBE_COLLISION_SCALE / 2), 0, island.scale.Z * (CUBE_COLLISION_SCALE / 2)),
                                                     new Vector3(island.scale.X * (CUBE_COLLISION_SCALE / 2), island.scale.Y * (CUBE_COLLISION_SCALE), island.scale.Z * (CUBE_COLLISION_SCALE / 2)));

            BBOcean = new BoundingBox(Vector3.Zero - new Vector3(ocean.scale.X * (CUBE_COLLISION_SCALE / 2), 0, ocean.scale.Z * (CUBE_COLLISION_SCALE / 2)),
                                                     new Vector3(ocean.scale.X * (CUBE_COLLISION_SCALE / 2), ocean.scale.Y * (CUBE_COLLISION_SCALE), ocean.scale.Z * (CUBE_COLLISION_SCALE / 2)));

            BBHighway = new BoundingBox(highway.position - new Vector3(highway.scale.X * (CUBE_COLLISION_SCALE / 2), 0, highway.scale.Z * (CUBE_COLLISION_SCALE / 2)),
                                          highway.position + new Vector3(highway.scale.X * (CUBE_COLLISION_SCALE / 2), highway.scale.Y * (CUBE_COLLISION_SCALE), highway.scale.Z * (CUBE_COLLISION_SCALE / 2)));

            BBHighway2 = new BoundingBox(highway2.position - new Vector3(highway2.scale.X * (CUBE_COLLISION_SCALE / 2), 0, highway2.scale.Z * (CUBE_COLLISION_SCALE / 2)),
                                           highway2.position + new Vector3(highway2.scale.X * (CUBE_COLLISION_SCALE / 2), highway2.scale.Y * (CUBE_COLLISION_SCALE), highway2.scale.Z * (CUBE_COLLISION_SCALE / 2)));

            for (int i = 0; i < NB_FIGHTER; i++)
            {
                BSFighter[i] = fighter[i].model.Meshes[0].BoundingSphere;
                BSFighter[i].Radius *= fighter[i].scale.X;
            }



            for (int i = 0; i < NB_BUILDINGX; i++)
            {
                for (int k = 0; k < NB_BUILDINGZ; k++)
                {
                    BBCube[i, k] = new BoundingBox(building[i, k].position - new Vector3((building[i, k].scale.X * (CUBE_COLLISION_SCALE / 2)), 0, (building[i, k].scale.Z * (CUBE_COLLISION_SCALE / 2))), //Vector minimum
                                                   building[i, k].position + new Vector3((building[i, k].scale.X * (CUBE_COLLISION_SCALE / 2)), (building[i, k].scale.Y * CUBE_COLLISION_SCALE), (building[i, k].scale.Z * (CUBE_COLLISION_SCALE / 2)))); //Vector maximum
                }
            }
        }

        void ScanForCollision()
        {
            BSJet.Center = jet.position;
            BSParachute.Center = parachute.position;

            if ((BSJet.Intersects(BBFloor)) || (BSJet.Intersects(BBOcean)) || (BSJet.Intersects(BBHighway)) || (BSJet.Intersects(BBHighway2)))
            {
                if (!jet.isExploding)
                {
                    Explosion(jet.position - (jet.jetBack * (jet.moveSpeed - 3)), true);
                }
                jet.Explode();

            }

            for (int i = 0; i < NB_BUILDINGX; i++)
            {
                for (int k = 0; k < NB_BUILDINGZ; k++)
                {
                    if (BSJet.Intersects(BBCube[i, k]))
                    {
                        if (!jet.isExploding)
                        {
                            Explosion(jet.position - (jet.jetBack * (jet.moveSpeed - 3)), true);
                        }
                        jet.Explode();
                    }
                    if (BSParachute.Intersects(BBCube[i, k]))
                    {
                        parachute.Reset(MAP_SIZE_X, (MAP_SIZE_X / 2), MAP_SIZE_Z, (MAP_SIZE_Z / 2));
                    }
                    for (int m = 0; m < jet.getMissilesCount(); m++)
                    {
                        if ((jet.launcher.MissileTab[m].missile.isFired) && (jet.launcher.MissileTab[m].missile.isActive))
                        {
                            BoundingSphere BSMissile = new BoundingSphere();
                            BSMissile.Center = jet.launcher.MissileTab[m].missile.position;
                            BSMissile.Radius *= jet.launcher.MissileTab[m].missile.scale.X;

                            if (BSMissile.Intersects(BBCube[i, k]))
                            {
                                jet.launcher.MissileTab[m].missile.isActive = false;
                                Explosion(jet.launcher.MissileTab[m].missile.position, true);
                            }
                        }
                    }
                }
            }
            for (int i = 0; i < NB_FIGHTER; i++)
            {
                BSFighter[i].Center = fighter[i].position;
                if (BSJet.Intersects(BSFighter[i]))
                {
                    if (!jet.isExploding)
                    {
                        Explosion(jet.position - (jet.jetBack * (jet.moveSpeed - 3)), true);
                    }
                    jet.Explode();
                }
            }

            if (BSParachute.Center.Y < 0)
            {
                parachute.Reset(MAP_SIZE_X, (MAP_SIZE_X / 2), MAP_SIZE_Z, (MAP_SIZE_Z / 2));
            }

            if (BSJet.Intersects(BSParachute))
            {
                jet.launcher.ReloadMissiles();
                jet.nbMunition = 300;
                parachute.Reset(MAP_SIZE_X, (MAP_SIZE_X / 2), MAP_SIZE_Z, (MAP_SIZE_Z / 2));
                SoundManager.GetSound("jet.reload").Play();
            }


            for (int i = 0; i < jet.getMissilesCount(); i++)
            {
                if ((jet.launcher.MissileTab[i].missile.isActive) && (jet.launcher.MissileTab[i].missile.isFired))
                {
                    BoundingSphere BSMissile = new BoundingSphere();
                    BSMissile.Center = jet.launcher.MissileTab[i].missile.position;
                    BSMissile.Radius *= jet.launcher.MissileTab[i].missile.scale.X;

                    if ((BSMissile.Intersects(BBFloor)) || (BSMissile.Intersects(BBOcean)) || (BSMissile.Intersects(BBHighway)) || (BSMissile.Intersects(BBHighway2) || (BSMissile.Intersects(BSMissilesBorders))))
                    {
                        jet.launcher.MissileTab[i].missile.isActive = false;
                        Explosion(jet.launcher.MissileTab[i].missile.position, true);
                    }
                    else for (int j = 0; j < NB_FIGHTER; j++)
                        {
                            if (BSMissile.Intersects(BSFighter[j]))
                            {
                                jet.launcher.MissileTab[i].missile.isActive = false;
                                Explosion(jet.launcher.MissileTab[i].missile.position, true);
                                fighter[j].position = motherShip.position;
                                fighter[j].Killed();
                            }
                        }
                }
            }
        }

        #endregion

        #region Interface
        void InitInterface()
        {
            FullScreenRectangle.X = 0; FullScreenRectangle.Y = 0;
            FullScreenRectangle.Width = SCREEN_WIDTH; FullScreenRectangle.Height = SCREEN_HEIGHT;

            cursorRectangleAlt.X = 1090;
            cursorRectangleAlt.Width = 40; cursorRectangleAlt.Height = 16;

            cursorRectangleSpeed.X = 310;
            cursorRectangleSpeed.Width = 40; cursorRectangleSpeed.Height = 16;

            healthBarRectangle.X = 456; healthBarRectangle.Y = 816;
            healthBarRectangle.Height = 24;
            healthBarBaseRectangle.X = healthBarRectangle.X - 10; healthBarBaseRectangle.Y = healthBarRectangle.Y - 10;
            healthBarBaseRectangle.Width = 520; healthBarBaseRectangle.Height = 44;

            rectangleCrossair.Width = 121;
            rectangleCrossair.Height = 121;
            rectangleCrossair.X = (SCREEN_WIDTH / 2) - (rectangleCrossair.Width / 2);
            rectangleCrossair.Y = (SCREEN_HEIGHT / 2) - (rectangleCrossair.Height / 2);


            for (int i = 0; i < NB_FIGHTER; i++)
            {
                grayRectangleCursor[i].Width = 44;
                grayRectangleCursor[i].Height = 43;
            }
            blueRectangleCursor.Width = 44; blueRectangleCursor.Height = 43;
            redRectangleCursor.Width = 44; redRectangleCursor.Height = 43;

            interfaceColor = new Color(66, 231, 44);
            couleurDistanceInterface = new Color(255, 255, 255);
        }

        void LoadInterface()
        {
            HUD = Content.Load<Texture2D>("./Content/Textures/Interface/HUD");
            BlackFade = Content.Load<Texture2D>("./Content/Textures/Interface/BlackFade");
            cursorAlt = Content.Load<Texture2D>("./Content/Textures/Interface/pointeur");
            cursorSpeed = Content.Load<Texture2D>("./Content/Textures/Interface/pointeur2");
            redCursor = Content.Load<Texture2D>("./Content/Textures/Interface/curseurRouge");
            grayCursor = Content.Load<Texture2D>("./Content/Textures/Interface/curseurGris");
            blueCursor = Content.Load<Texture2D>("./Content/Textures/Interface/curseurBleu");
            healthBar = Content.Load<Texture2D>("./Content/Textures/Interface/HealthBar");
            healthBarBase = Content.Load<Texture2D>("./Content/Textures/Interface/HealthBarBase");
            crossair = Content.Load<Texture2D>("./Content/Textures/Interface/Viseur");
        }

        void UpdateInterface()
        {
            healthBarRectangle.Width = (jet.health * 5);

            for (int i = 0; i < NB_FIGHTER; i++)
            {
                Vector3 center = ScreenManager.GraphicsDevice.Viewport.Project(fighter[i].position,
                cam.projectionMatrix, cam.viewMatrix, Matrix.Identity);
                grayRectangleCursor[i].X = (int)center.X - (grayRectangleCursor[i].Width / 2);
                grayRectangleCursor[i].Y = (int)center.Y - (grayRectangleCursor[i].Height / 2);

                if (jet.target != Vector3.Zero)
                {
                    if (!(viewFrustum.Contains(jet.target) == ContainmentType.Contains))
                        jet.target = Vector3.Zero;
                }

                if (grayRectangleCursor[i].Intersects(rectangleCrossair))
                {

                    jet.target = fighter[i].position;


                    if (!(viewFrustum.Contains(jet.target) == ContainmentType.Contains))
                        jet.target = Vector3.Zero;
                }
            }

            if (jet.target != Vector3.Zero)
            {
                Vector3 center = ScreenManager.GraphicsDevice.Viewport.Project(jet.target,
                    cam.projectionMatrix, cam.viewMatrix, Matrix.Identity);
                redRectangleCursor.X = (int)center.X - (redRectangleCursor.Width / 2);
                redRectangleCursor.Y = (int)center.Y - (redRectangleCursor.Height / 2);
            }


            {
                Vector3 center = ScreenManager.GraphicsDevice.Viewport.Project(parachute.position,
                    cam.projectionMatrix, cam.viewMatrix, Matrix.Identity);
                blueRectangleCursor.X = (int)center.X - (blueRectangleCursor.Width / 2);
                blueRectangleCursor.Y = (int)center.Y - (blueRectangleCursor.Height / 2);
            }
        }

        void DrawInterface()
        {
            spriteBatch.Draw(crossair, rectangleCrossair, null, Color.White, 0, new Vector2(0, 0), SpriteEffects.None, 1);

            for (int i = 0; i < NB_FIGHTER; i++)
            {
                if (viewFrustum.Intersects(BSFighter[i]))
                {
                    spriteBatch.Draw(grayCursor, grayRectangleCursor[i], null, Color.White, 0, new Vector2(0, 0), SpriteEffects.None, 1);
                    spriteBatch.DrawString(distanceInterfaceFont, "" + Util.GetDistance(jet.position, fighter[i].position), new Vector2(grayRectangleCursor[i].X + 12, grayRectangleCursor[i].Y), couleurDistanceInterface);
                }
            }
            if (viewFrustum.Intersects(BSParachute))
            {
                spriteBatch.Draw(blueCursor, blueRectangleCursor, null, Color.White, 0, new Vector2(0, 0), SpriteEffects.None, 1);
                spriteBatch.DrawString(distanceInterfaceFont, "" + Util.GetDistance(jet.position, parachute.position), new Vector2(blueRectangleCursor.X + 12, blueRectangleCursor.Y), couleurDistanceInterface);
            }
            if (jet.target != Vector3.Zero)
            {
                if (viewFrustum.Contains(jet.target) == ContainmentType.Contains)
                {
                    spriteBatch.Draw(redCursor, redRectangleCursor, null, Color.White, 0, new Vector2(0, 0), SpriteEffects.None, 1);
                    spriteBatch.DrawString(distanceInterfaceFont, "" + Util.GetDistance(jet.position, parachute.position), new Vector2(redRectangleCursor.X + 12, redRectangleCursor.Y), couleurDistanceInterface);
                }
            }


            spriteBatch.Draw(HUD, FullScreenRectangle, Color.White);

            spriteBatch.Draw(healthBar, healthBarRectangle, Color.White);
            spriteBatch.Draw(healthBarBase, healthBarBaseRectangle, Color.White);

            spriteBatch.DrawString(interfaceFont, "FPS: " + frameRate, new Vector2(270, 106), interfaceColor);
            spriteBatch.DrawString(interfaceFont, scoreKeeper.Score.ToString() + " pts", new Vector2(1013, 106), interfaceColor);
            spriteBatch.DrawString(interfaceFont, "MG: " + jet.nbMunition, new Vector2(1023, 718), interfaceColor);
            spriteBatch.DrawString(interfaceFont, "MSSL: " + jet.getMissilesCount(), new Vector2(1023, 788), interfaceColor);

            //vitesse//
            spriteBatch.Draw(cursorSpeed, cursorRectangleSpeed, Color.White);
            spriteBatch.DrawString(interfaceFont, "SPEED", new Vector2(202, 210), interfaceColor);
            spriteBatch.DrawString(interfaceFont, "200", new Vector2(165, 235), interfaceColor);
            spriteBatch.DrawString(interfaceFont, "150", new Vector2(165, 335), interfaceColor);
            spriteBatch.DrawString(interfaceFont, "100", new Vector2(165, 435), interfaceColor);
            spriteBatch.DrawString(interfaceFont, "50", new Vector2(165, 535), interfaceColor);
            spriteBatch.DrawString(interfaceFont, "0", new Vector2(165, 635), interfaceColor);
            spriteBatch.DrawString(interfaceFont, "MPHx10", new Vector2(243, 660), interfaceColor);
            ///////////

            //altitude//
            spriteBatch.Draw(cursorAlt, cursorRectangleAlt, Color.White);
            spriteBatch.DrawString(interfaceFont, "ALT", new Vector2(1138, 210), interfaceColor);
            spriteBatch.DrawString(interfaceFont, "20", new Vector2(1225, 235), interfaceColor);
            spriteBatch.DrawString(interfaceFont, "15", new Vector2(1225, 335), interfaceColor);
            spriteBatch.DrawString(interfaceFont, "10", new Vector2(1225, 435), interfaceColor);
            spriteBatch.DrawString(interfaceFont, "5", new Vector2(1225, 535), interfaceColor);
            spriteBatch.DrawString(interfaceFont, "0", new Vector2(1225, 635), interfaceColor);
            spriteBatch.DrawString(interfaceFont, "FTx100", new Vector2(1138, 660), interfaceColor);
            ////////////

            spriteBatch.Draw(BlackFade, FullScreenRectangle, Color.White);
        }
        #endregion

        #region Explosion

        void DrawExplosions(Matrix view, Matrix projection)
        {
            explosionParticles.SetCamera(view, projection);
            explosionSmokeParticles.SetCamera(view, projection);
            projectileTrailParticles.SetCamera(view, projection);
        }


        private void Explosion(Vector3 pos, bool withVolume)
        {
            explosionParticles.AddParticle(pos, Vector3.Zero);
            explosionSmokeParticles.AddParticle(pos, Vector3.Zero);
            if (withVolume)
                SoundManager.GetSound("explosion").Play();
        }


        #endregion


        void DrawArtillery()
        {
            artillerySteps++;
            if (artillerySteps >= ARTILLERY_RATE)
            {
                Random rand = new Random();
                int randX;
                int randY;
                int randZ;

                randX = rand.Next(MAP_SIZE_X) - (MAP_SIZE_X / 2);
                randY = rand.Next(10000) + 10000;
                randZ = rand.Next(MAP_SIZE_Z) - (MAP_SIZE_Z / 2);

                Explosion(new Vector3(randX, randY, randZ), false);
                artillerySteps = 0;
            }
        }
    }
}