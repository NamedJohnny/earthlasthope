﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Net;
using Microsoft.Xna.Framework.Storage;

namespace EarthsLastHope
{
    public abstract class GameObject
    {
        public Model model = null;
        public Texture2D texture = null;
        public Vector3 position = Vector3.Zero;
        public Vector3 velocity = Vector3.Zero;
        public Quaternion rotation = Quaternion.Identity;

        public Vector3 scale;
        public bool active = true;


        public GameObject(Vector3 position)
            : this(position, new Vector3(1,1,1))
        {
            this.position = position;
        }

        public GameObject(Vector3 position, Vector3 scale)
            :this(position, scale, Quaternion.Identity)
        {
            this.position = position;
            this.scale = scale;
        }

        public GameObject(Vector3 position, Vector3 scale, Quaternion rotation)
        {
            this.position = position;
            this.scale = scale;
            this.rotation = rotation;
        }

        public virtual void Update() { }

        public virtual void Draw(Camera cam)
        {
            foreach (ModelMesh mesh in model.Meshes)
            {
                foreach (BasicEffect effect in mesh.Effects)
                {
                    if (texture != null)
                    {
                        effect.TextureEnabled = true;
                        effect.Texture = texture;
                    }

                    effect.PreferPerPixelLighting = false;
                    effect.World = Matrix.CreateFromYawPitchRoll(rotation.Y, rotation.X, rotation.Z) * Matrix.CreateScale(scale) * Matrix.CreateTranslation(position);
                    effect.Projection = cam.projectionMatrix;
                    effect.View = cam.viewMatrix;

                    effect.DirectionalLight0.Enabled = true;
                    effect.DirectionalLight0.DiffuseColor = new Vector3(1.0f, 1.0f, 1.0f);
                    effect.DirectionalLight0.SpecularColor = new Vector3(0.2f, 0.2f, 0.2f);
                    effect.AmbientLightColor = new Vector3(0.7f, 0.7f, 0.7f);
                    effect.SpecularPower = 10.0f; effect.SpecularColor = new Vector3(0.6f, 0.6f, 0.3f);
                    effect.DirectionalLight0.Direction = Vector3.Normalize(position);

                    effect.EmissiveColor = new Vector3(0.0f, 0.0f, 0.0f);
                    effect.LightingEnabled = true;
                    effect.FogEnabled = true;
                    effect.FogColor = new Vector3(1.0f, 1.0f, 1.0f);
                    effect.FogEnd = 100000;
                } mesh.Draw();
            }
        }
    }
}

