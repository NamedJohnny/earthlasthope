﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Audio;

namespace EarthsLastHope
{
    public class SoundManager
    {
        Dictionary<string, SoundEffect> sounds;
        private static SoundManager instance;

        private SoundManager()
        {
            sounds = new Dictionary<string, SoundEffect>();
        }

        private static SoundManager GetInstance()
        {
            if (instance == null)
                instance = new SoundManager();

            return instance;
        }

        public static void AddSound(string name, SoundEffect sound)
        {
            if (!GetInstance().sounds.ContainsKey(name))
                GetInstance().sounds.Add(name, sound);
        }

        public static SoundEffect GetSound(string name)
        {
            if (GetInstance().sounds.ContainsKey(name))
                return GetInstance().sounds[name];

            return null;
        }

        public static void Clear()
        {

            if (GetInstance().sounds != null) 
                GetInstance().sounds.Clear();

        }
    }
}
