﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EarthsLastHope.Observer
{
    public interface ISubject
    {
        void RegisterObserver(IObserver observer);
        void UnregisterObserver(IObserver observer);
        void NotifyObservers();
    }
}
