﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EarthsLastHope.Observer
{
    public interface IObserver
    {
        void Update(int pointWorth);
    }
}
