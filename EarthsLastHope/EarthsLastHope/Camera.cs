﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace EarthsLastHope
{
    public class Camera
    {
        public Matrix viewMatrix;
        public Matrix projectionMatrix;

        Quaternion cameraRotation;

        public Vector3 position;
        protected Vector3 cameraLookAt;
        protected Vector3 camup;
        protected Vector3 cambak;

        public Vector3 offset;

        GameObject target;

        public Camera(GameObject target, Vector3 offset, Matrix viewMatrix, Matrix projectionMatrix){
            this.target = target;
            this.offset = offset;
            this.viewMatrix = viewMatrix;
            this.projectionMatrix = projectionMatrix;
            cameraRotation = Quaternion.Identity;
        }

        public void Update() {
            cameraRotation = Quaternion.Lerp(cameraRotation, target.rotation, 0.1f);

            Vector3 campos = new Vector3(offset.X, target.scale.Y + offset.Y, target.scale.Z + offset.Z);
            //Vector3 campos = new Vector3(0, target.scale.Y * 10, target.scale.Z * 40 + offSet.Z);
            campos = Vector3.Transform(campos, Matrix.CreateFromQuaternion(cameraRotation));
            campos += target.position;
            position = campos;

            camup = new Vector3(0, 1, 0);
            camup = Vector3.Transform(camup, Matrix.CreateFromQuaternion(cameraRotation));

            cambak = new Vector3(0, 0, -1);
            cambak = Vector3.Transform(cambak, Matrix.CreateFromQuaternion(cameraRotation));

            cameraLookAt = target.position + (camup * target.scale * 10);

            position = campos;
            viewMatrix = Matrix.CreateLookAt(campos, cameraLookAt, camup);
        }
    }
}
