﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Microsoft.Xna.Framework;
using EarthsLastHope;

namespace UnitTests
{
    [TestFixture]
    public class EarthsLastHopeTestCases
    {
        Game1 game = null;

        [TestFixtureSetUp]
        public void SetUp()
        {
            game = new Game1();
            game.Activated += new EventHandler<EventArgs>(game_Activated);
            game.Run();
        }

        void game_Activated(object sender, EventArgs e)
        {
            game.Exit();
        }

        [TestFixtureTearDown]
        public void TeardownFixture()
        {
            game = null;
        }

        [Test]
        public void GameStartup()
        {
            Assert.IsNotNull(game);
            Assert.IsNotNull(game.GraphicsDevice);
            Assert.IsNotNull(game.Content);
        }

    }
}
